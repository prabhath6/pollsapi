from rest_framework.test import APITestCase, APIRequestFactory
from polls import views
from django.contrib.auth import get_user_model


class TestPoll(APITestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.view = views.PollList.as_view()
        self.uri = '/polls/'
        self.user = self.get_user()

    @staticmethod
    def get_user():
        user = get_user_model()
        return user.objects.create_user(
            username='nate.silver2',
            email="test@test.com",
            password='FiveThirtyEight'
        )

    def test_list(self):
        request = self.factory.get(self.uri)
        request.user = self.user
        response = self.view(request)

        self.assertEqual(response.status_code, 200, f'Expected status code 200 but got {response.status_code} instead.')
