from .views import PollList, PollDetailsList, ChoiceList, CreateVote, UserList, LoginView
from django.urls import path
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Polls API')


urlpatterns = [
    path('polls/', PollList.as_view(), name='poll'),
    path('polls/<int:pk>', PollDetailsList.as_view(), name='poll_details'),
    path('polls/<int:poll_pk>/choice/', ChoiceList.as_view(), name='choice_list'),
    path('polls/<int:poll_pk>/choice/<int:choice_pk>/vote/', CreateVote.as_view(), name='vote'),
    path('users/', UserList.as_view(), name='user_list'),
    path('login/', LoginView.as_view(), name='login'),
    path('swagger-docs/', schema_view)
]
