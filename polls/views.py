from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import authenticate, PermissionDenied
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_401_UNAUTHORIZED
from .models import Poll, Choice, Vote
from .serializers import PollSerializer, ChoiceSerializer, VoteSerializer, UserSerializer


class PollList(generics.ListCreateAPIView):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer

    def destroy(self, request, *args, **kwargs):
        poll = Poll.objects.get(pk=self.kwargs["pk"])
        if not request.user == poll.created_by:
            raise PermissionDenied("You can not delete this poll.")
        return super().destroy(request, *args, **kwargs)


class PollDetailsList(generics.RetrieveDestroyAPIView):
    queryset = Poll.objects.all()
    serializer_class = PollSerializer


class ChoiceList(generics.ListCreateAPIView):
    def get_queryset(self):
        return Choice.objects.filter(poll_id=self.kwargs["poll_pk"])
    serializer_class = ChoiceSerializer

    def post(self, request, *args, **kwargs):
        poll = Poll.objects.get(pk=self.kwargs["pk"])
        if not request.user == poll.created_by:
            raise PermissionDenied("You can not create choice for this poll.")
        return super().post(request, *args, **kwargs)


class CreateVote(APIView):

    def get_object(self, poll_id, choice_id):
        if poll_id is None or choice_id is None:
            return None
        data_result = Vote.objects.filter(poll_id=poll_id).filter(choice_id=choice_id)
        return data_result

    # This gives name of the votes.
    def get(self, request, poll_pk, choice_pk):
        data_result = self.get_object(poll_pk, choice_pk)
        data = []
        for vote_data in data_result:
            data.append({
                    'choice': vote_data.choice.choice_text,
                    'poll': vote_data.poll.question,
                    'voted_by': vote_data.voted_by.username
                })
        return Response(data, status=HTTP_200_OK)

    def post(self, request, poll_pk, choice_pk):
        voted_by = request.data.get("voted_by")
        data = {'choice': choice_pk, 'poll': poll_pk, 'voted_by': voted_by}
        serializer = VoteSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    # This works for generics.ListView
    # This returns just ids
    # def get_queryset(self):
    #     queryset = Vote.objects.filter(poll_id=self.kwargs['poll_pk']).filter(choice_id=self.kwargs['choice_pk'])
    #     return queryset
    # serializer_class = VoteSerializer


class UserList(generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer


class LoginView(APIView):
    permission_classes = ()

    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(username=username, password=password)

        if user:
            return Response({'auth_token': user.auth_token.key}, status=HTTP_200_OK)
        else:
            return Response({'error': "Wrong Credentials"}, status=HTTP_401_UNAUTHORIZED)
